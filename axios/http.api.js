import service from './http.interceptor.js'

// 封装后端api接口
const api = {
    // 话术搜索
    getLoveChat: params => service.post('/open/getLoveChat', params),
    getScoreMaxLoveChat: () => service.post('/open/getScoreMaxLoveChat'),
    getBanner: () => service.post('/open/getBanner'),
    // 浪漫情话推荐
    getRecommendLove: () => service.post('/open/getRecommendLove'),
    loveScore: params => service.post('/open/loveScore', params),
	// 顶部公告
	barContent: params => service.post('/open/getBar', params),
}

export default api
