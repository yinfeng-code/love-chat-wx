//封装axios拦截器
import axios from './axios-adaptor.js'

const service = axios.create({
    baseURL: 'https://yin-feng.top',
    timeout: 3000, // request timeout
    crossDomain: true,
    headers: {
        'Content-Type': 'application/json; charset=utf-8'
    }
})
// 添加请求拦截器 
service.interceptors.request.use(
    config => {
        config.headers['source'] = 'wx'
        return config
    },
    error => {
        return Promise.reject(error)
    }
)
// 添加响应拦截器 
service.interceptors.response.use(
    response =>
        response.data,
    error => {
        return Promise.resolve(error)
    }
)
export default service
