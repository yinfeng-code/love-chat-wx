#Love chat wx


#Welcome to my code repository!


I am a personal developer, mainly engaged in front-end and back-end development work. This code repository is a love story WeChat mini program that includes daily recommended love stories and love stories. 


On my personal homepage, you can find my personal projects and some open source projects. These projects were developed by me in the process of work and learning, including front-end projects, back-end projects, and some tool classes. These projects are all shared in an open source manner, and you can use and modify them for free. 


If you are interested in my project, you can leave me a message in the corresponding warehouse, and I will reply to your questions as soon as possible. At the same time, you can also submit a PR to me and provide your suggestions and ideas. 


##Project List


###Personal projects


-[x] My Personal Website:<https://yin-feng.top>


###Open source projects


-[x] love chat wx (Love Script WeChat mini program, including daily recommended love stories and love scripts):<https://gitee.com/yinfeng-code/love-chat-wx.git>

-[x] Java robot (Java robot project, mainly used for receiving, processing, and sending QQ messages):<https://gitee.com/yinfeng-code/java-robot.git>

-[x] official web (personal studio's promotional homepage, mainly used to showcase my skills and abilities, and enhance business value and influence):<https://gitee.com/yinfeng-code/official-web.git>

-[x] Spring boot test (spring boot test project, suitable for beginners):<https://gitee.com/yinfeng-code/test.git>



##Instructions for use


The usage instructions for this code repository are as follows: 


-Firstly, enter the root directory of the code repository from the command line. 

-If you want to view the README of the projectMD file, you can use the following command: 

```ssh

git clonehttps://gitee.com/yinfeng-code/love-chat-wx.git

cd love chat wx

cat READMEMD

```


-If you want to view the source code of the project, you can use the following command: 

```ssh

git clonehttps://gitee.com/yinfeng-code/love-chat-wx.git

cd love chat wx

git checkout - b<branch_Name>

```


Among them, '<branch'_Name>'is the branch name of the project you want to view. 

You can use the following command to list all available branch names: 

```ssh

git branch -- list

```


-If you want to submit code to the warehouse, you can use the following command: 

```ssh

git add

git commit - m "Your commit message here" 

git push origin<branch_Name>

```


Among them, '<branch'_Name>'is the name of the branch you want to push. 